/*
 * Copyright (c) 2017. Dean Elzinga. An early, exploratory work to gain insight into mass-matching
 * algorithms using finite state automata or finite state transducers.
 *
 * FIXME: Move tests into jUnit. Write jUnit tests.
 * FIXME: Remove cheesy output statements.
 *
 * OPTIMIZATIONS
 * TODO: Presort words, prefixes. Use exact counts of node branching, not current dynamic approach.
 * TODO: Store words & prefixes as indices into array or similar.
 * TODO: Store node outputs as Roaring Bitmaps, representing subsets of dict words or dict prefixes.
 *
 * GENERALIZATIONS, CONCURRENCY
 * TODO: FUZZY SEARCH--Explore keeping multiple output subsets at each node, for each edit distance
 * less than or equal to some N. (Roaring Bitmaps to efficiently represent these sets.) In other
 * words, picture the original algorithm as the N=0 case. Imagine what it might take to generalize
 * to N=1 using additional output sets at each node, possibly expanding the FSA by some sort of
 * replicate-modify transformation.
 *
 * TODO: Generalize haystack string to character stream.
 *
 * TODO: For enormous haystacks and relatively short-word dictionaries, consider parallelizing the
 * search by splitting the haystack and knitting the independent results back together. Care must
 * be taken to add a little extra from both sides at each split, equal to 1/2 the length of the
 * longest dictionary word.
 *
 * TODO: For enormous dictionaries, consider parallelizing by splitting the dictionary by prefix.
 * As long as every node is in the same segment as its parents and fail nodes (possibly a delicate
 * operation, maybe impossible) it seems possible at first glance to construct separate tries.
 * Regardless of fine-structure fiddling, it's ALWAYS possible to simply break the dictionary up,
 * run the match against each sub-dictionary, then combine the results. This has a cost, which must
 * be weighed against the potential speedup.
 *
 */

package com.deanelzinga.searchsort;

// Fastutils: High-performance primitive collections. Fast, popular, free under Apache license.

import it.unimi.dsi.fastutil.chars.Char2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;

import java.util.*;


/**
 * Implements the Aho-Corasick multiple-string searching algorithm, which solves the "needles
 * in a haystack" problem. Aho-Corasick, unlike its string-search kin, Boyer-Moore and
 * Knuth-Morris-Pratt, finds all instances of multiple "needles" at once--all the words in a given
 * dictionary--in a single pass through the "haystack".
 * <p>
 * Aho-Corasick accomplishes this in linear time and space, based on the size of the dictionary, the
 * length of the haystack, and the number of matches: O(D + H + M).
 * <p>
 * Aho-Corasick is based on a type of trie that can also be seen as a finite state automaton
 * (FSA). The trie only needs to be set up once for a given dictionary. It can be reused for
 * multiple haystack inputs for no additional cost. Once set up, this trie is also immutable during
 * the matching phase. Because of immutability of the dictionary trie, Aho-Corasick is ripe for
 * parallel access.
 */
public class AhoCorasick {
    private Set<String> dict;
    private Node root;
    private int size = 0;


    /**
     * Initializes an Aho Corasick trie with a dictionary of words or "needles". This trie can
     * be used and reused on subsequent "haystack" strings in order to find all occurrences of all
     * words in the dictionary.
     *
     * @param dict the dictionary of words or "needles" to find in any input haystack
     */
    public AhoCorasick(Collection<String> dict) {
        if (dict == null)
            throw new NullPointerException("Requires non-null dictionary");
        this.dict = new HashSet<>(dict.size());
        this.dict.addAll(dict);
        buildTrie();
        buildFails();
    }

    /*
    Constructs the trie. Besides the usual "forward" transitions in the trie (for when a letter
    in the haystack matches a letter from the dictionary), each node also has...
    - A list of outputs
    - A fail link, 1 per node, to retry matching the current haystack letter at a shorter prefix
      of words from the dictionary.

    The root node corresponds to the empty string, or the start node of a finite state automaton.
    First letters of dictionary words are represented by child nodes of the root node. Second
    letters are their child nodes, and so on.

    In other words, each node corresponds 1-to-1 with a prefix of some word in the dictionary,
    from the empty string (for the root node) all the way to terminating at the longest words.

    Example: Dictionary, {"sea", "see", "set", "sex"}...
    has prefixes, {"", "s", "se", "sea", "see", "set", "sex"}, so we expect 7 nodes in its trie.
    */
    private void buildTrie() {
        root = new Node("");
        ++size;

        // For every letter in every dictionary word, step through the trie, creating new nodes
        // as necessary.
        for (String word : dict) {
            Node node = root;
            for (int i = 0; i < word.length(); ++i) {
                char c = word.charAt(i);
                Node child = node.childNodes.get(c);
                if (child == null) {
                    ++size;
                    child = new Node(node.prefix.concat(String.valueOf(c)));
                    child.parent = node;
                    node.childNodes.put(c, child);
                }
                node = child;
            }

            // If a word terminates at this node, add it to node outputs:
            node.outputs.add(word);
        }
    }

    /*
    Constructs the "fail" links (FSA transitions): for when we fail to match an input letter.
    This is probably the trickiest part of the initialization to visualize from reading code or
    explanations. See YouTube, such as David Esposito's "AhoCorasickBuildFail" video.
    */
    private void buildFails() {
        // Root fails to root:
        root.fail = root;

        // Do a level-order traversal of trie. Like any tree, using a queue allows us to traverse the
        // the trie nodes in breadth-first order (aka level order).
        Queue<Node> buildFailsQ = new ArrayDeque<>();

        // Root childNodes also fail to root, then add to queue:
        for (Node next : root.childNodes.values()) {
            next.fail = root;
            buildFailsQ.add(next);
        }

        // For every node on queue
        while (!buildFailsQ.isEmpty()) {
            // Get next node from queue
            Node parent = buildFailsQ.poll();
            System.out.println(parent);

            // For every child
            System.out.println(parent.childNodes.keySet());
            for (char key : parent.childNodes.keySet()) {
                Node child = parent.childNodes.get(key);

                // By default, fail to root.
                child.fail = root;

                // MAIN IDEA: Starting at the parent's fail node,
                Node parentFail = parent.fail;

                // Find the nearest ancestor with same key.
                while (!parentFail.childNodes.containsKey(key) && parentFail != root) {
                    parentFail = parentFail.parent;
                }
                Node childFail = parentFail.childNodes.get(key);

                // If this ancestor has a child with that key, then fail to
                // THAT CHILD and copy its outputs here:
                if (childFail != null) {
                    child.fail = childFail;
                    child.outputs.addAll(childFail.outputs);
                }

                buildFailsQ.add(child);
            }
        }
    }


    /**
     * @param haystack the string to search in
     * @return a map. The keys are a subset of the dictionary, every word found in the haystack.
     * The values are lists of all occurrences for all of those words, expressed as the positions
     * in the haystack where each word is found.
     */
    public Map<String, List<Integer>> match(String haystack) {
        HashMap<String, List<Integer>> match = new HashMap<>();
        int len = haystack.length();

        int i = 0;
        Node node = root;
        while (i < len) {
            char key = haystack.charAt(i);
            Node child = node.childNodes.get(key);

            // CASE: Node has child haystack[i]: Save outputs and increment i.
            if (child != null) {
                node = child;
                for (String output : node.outputs) {
                    List<Integer> locations = match.get(output);
                    if (locations == null) {
                        locations = new IntArrayList();
                        locations.add(i - output.length() + 1);
                        match.put(output, locations);
                    } else {
                        // Add this "hit" location to list: i - output.length + 1
                        locations.add(i - output.length() + 1);
                    }
                }
                ++i;
            }

            // CASE: Starting at (or failed to) root & found nothing. Advance to next letter.
            else if (node == root) {
                ++i;
            }

            // CASE: Fail up the trie to re-try match. Don't give up on this haystack letter yet!
            else {
                node = node.fail;
            }
        }
        return match;
    }

    public int size() { return size; }

    private static class Node {
        final String prefix;
        Set<String> outputs;
        Map<Character, Node> childNodes;
        Node fail;
        Node parent;

        Node(String prefix) {
            if (prefix == null || prefix.equals("")) {
                this.prefix = "";
            } else {
                this.prefix = prefix;
            }
            this.outputs = new HashSet<>();
            this.childNodes = new Char2ObjectOpenHashMap<>();
            this.fail = null;
        }

        @Override
        public String toString() {
            return prefix + "{" +
                    "outputs=" + outputs +
                    ", childNodes=" + childNodes.keySet() +
                    ", fail=\"" + fail.prefix + '\"' +
                    ", parent=\"" + parent.prefix + '\"' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            if (prefix != null ? !prefix.equals(node.prefix) : node.prefix != null) return false;
            if (outputs != null ? !outputs.equals(node.outputs) : node.outputs != null)
                return false;
            if (childNodes != null ? !childNodes.equals(node.childNodes) : node.childNodes != null)
                return false;
            if (fail != null ?
                    (fail != node.fail)  // Avoid infinite loops, don't use equals().
                    : node.fail != null) return false;
            return parent != null ? parent.equals(node.parent) : node.parent == null;
        }

        @Override
        public int hashCode() {
            int result = prefix != null ? prefix.hashCode() : 0;
            result = 31 * result + (outputs != null ? outputs.hashCode() : 0);
            result = 31 * result + (childNodes != null ? childNodes.hashCode() : 0);
            result = 31 * result + (fail != null ? fail.hashCode() : 0);
            result = 31 * result + (parent != null ? parent.hashCode() : 0);
            return result;
        }
    }


    // Helper function not yet used: Potentially remove dependency on parent link, at some cost.
    private Node nodeOf(String prefix) {
        Node node = root;
        int len = prefix.length();
        int i = 0;
        while (i < len) {
            int c = prefix.charAt(i);
            node = node.childNodes.get(c);
        }
        return node;
    }


    public static void main(String[] args) {
        System.out.println("The Aho-Corasick string-search algorithm:");
        System.out.println("Finds all occurrences of all dictionary words in an input haystack.");
        System.out.println();
        Set<String> abacab = new HashSet<>(
                Arrays.asList("a", "ab", "bc", "aab", "aac", "bd")
        );
        Set<String> seeSex = new HashSet<>(
                Arrays.asList("he", "sea", "see", "set", "sex", "she")
        );
        List<Set<String>> dicts = Arrays.asList(abacab, seeSex);

        List<String> haystacks = Arrays.asList(
                "abacadabraabaacaadaabraa",
                "heorsheorhesellsseashellsbytheseashore");

        for (int i = 0; i < dicts.size(); ++i) {
            if (i >= haystacks.size()) break;
            Set<String> dict = dicts.get(i);
            System.out.println(dict.toString());
            AhoCorasick ahoCorasick = new AhoCorasick(dict);
            System.out.printf("size: %d%n", ahoCorasick.size());

            String haystack = haystacks.get(i);
            Map<String, List<Integer>> match = ahoCorasick.match(haystack);
            System.out.println(match);
            System.out.println();
        }
    }
}
