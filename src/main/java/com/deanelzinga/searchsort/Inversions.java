package com.deanelzinga.searchsort;

import java.util.*;

/**
 * Two different ways of computing the number of inversions for each item in an array, that is
 * the number of greater items at lower index
 */
public class Inversions {
    static <T extends Comparable<T>> int[] sortedIndex2(T[] array) {
        Integer[] invertedIndex = new Integer[array.length];
        for (int i = 0; i < invertedIndex.length; ++i) {
            invertedIndex[i] = i;
        }

        Arrays.sort(invertedIndex, Comparator.comparing(i -> array[i]));
        int[] sortedIndex = new int[invertedIndex.length];
        for (int j = 0; j < invertedIndex.length; ++j) {
            sortedIndex[j] = invertedIndex[j];
        }
        return sortedIndex;
    }

    static <T extends Comparable<T>> int[] sortedIndex(T[] array) {
        Map<T, List<Integer>> inverted;
        inverted = new HashMap<>(array.length);
        for (int i = 0; i < array.length; ++i) {
            T t = array[i];
            List<Integer> list = inverted.get(t);
            if (list == null) {
                list = new ArrayList<>(2);
                list.add(i);
                inverted.put(t, list);
            } else {
                list.add(i);
            }
        }
        T[] sorted = Arrays.copyOf(array, array.length);
        Arrays.sort(sorted);
        int[] sortedIndex = new int[sorted.length];

        Map<T, Integer> posMap = new HashMap<>(inverted.size());
        for (int j = 0; j < sorted.length; ++j) {
            T s = sorted[j];
            Integer Pos = posMap.get(s);
            int pos;
            if (Pos == null) {
                pos = 0;
                posMap.put(s, 1);
            } else {
                pos = Pos;
                posMap.put(s, pos + 1);
            }
            List<Integer> list = inverted.get(s);
            if (list == null)
                throw new IllegalStateException("Sorted array seems to contain object not in original");
            if (list.size() <= pos)
                throw new IllegalStateException("Sorted array seems to contain more copies of object than in original");
            int i = list.get(pos);  // original i for this j
            sortedIndex[j] = i;
        }
        return sortedIndex;
    }

    public static void main(String[] args) {
        Double[] test = new Double[]{0., 1., .5, 4.};
        System.out.println(Arrays.toString(sortedIndex(test)));
        System.out.println(Arrays.toString(sortedIndex2(test)));
    }
}
