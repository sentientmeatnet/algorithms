package com.deanelzinga.searchsort;


import java.util.Arrays;

public class Merge {
  private Merge() {}


  /**
   * Merge-sort array in place AND return number of inversions.
   * @param array
   * @param <T>
   * @return
   */
  public static <T extends Comparable<T>> int sortInversions(T[] array) {
    int len = array.length;
    T[] copy = Arrays.copyOf(array, len);
    int inversions = 0;
    for (int sz = 1; sz < len; sz = sz + sz) {
      for (int lo = 0; lo < len - sz; lo += sz + sz) {
        inversions += mergeInversions(array, copy, lo, lo + sz - 1,
            Math.min(lo + sz + sz - 1, len - 1));
      }
    }
    return inversions;
  }


  static <T extends Comparable<T>> int mergeInversions(T[] a1, T[] a2, int lo, int mid, int hi) {
    System.arraycopy(a1, lo, a2, lo, hi - lo + 1);
    int inversions = 0;
    int i = lo, j = mid + 1;
    for (int k = lo; k <= hi; ++k) {
      if (i > mid) {
        a1[k] = a2[j++];
      } else if (j > hi) {
        a1[k] = a2[i++];
      } else if (a2[j].compareTo(a2[i]) < 0) {
        a1[k] = a2[j++];
        // ONLY magic needed to accumulate inversions -- when adding from right half, copy[j],
        // just accumulate how many remain in the left half:
        inversions += mid - i + 1;
      } else {
        a1[k] = a2[i++];
      }
    }
    return inversions;
  }


  public static void main(String[] args) {
    Comparable[][] tests = new Comparable[][]{
        new Integer[]{4, 0, 1, 2, 3},
        new Integer[]{4, 3, 2, 1, 0},
        new Double[]{4., 2., 1., 3., 0.}
    };

    for (Comparable[] test : tests) {
      int inversions = sortInversions(test);
      System.out.println(Arrays.toString(test));
      System.out.println(inversions);
    }
  }
}
