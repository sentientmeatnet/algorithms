# Algorithms demo: currently of Aho-Corasick #

### Compile & create executable jar ###
1. Clone the repo and change to its root directory.
2. Compile & package using a Maven build.
3. Run the executable jar in the target directory.
```
#!bash
mvn clean package
java -jar target/algorithms*jar-with-dependencies.jar
```

Dean Elzinga, dcelzinga (over at gmail)